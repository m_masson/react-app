const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const createNews = (lastNews => {
    return admin.firestore().collection('lastnews')
        .add(lastNews)
        .then(doc => console.log('News added', doc));
})

// Cloud Functions for Notification System
// Trigger on New Post
// When post is created in the collection 'posts', fire a callback function
exports.postCreated = functions.firestore.document('posts/{postId}').onCreate(doc => {
    const post = doc.data();
    // Add this object as notification collection in database
    const news = {
        content: 'added a new post',
        user: `${post.authorFirstName} ${post.authorLastName}`,
        time: admin.firestore.FieldValue.serverTimestamp()
    }
    // Add news as new document
    return createNews(news);
})