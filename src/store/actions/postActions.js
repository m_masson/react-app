import { storage } from '../../config/fbConfig'

export const createPost = (post) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        // make async call to database
        const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;

        const storageRef = storage.ref();
        const file = post.file;
        const uploadTask = storageRef.child(`posts/${file.name}`).put(file);
        uploadTask.on('state_changed', function(snapshot) {
            console.log("progress");
        }, function(error) {
            console.log(error);
        }, function() {
            uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                console.log('File available at', downloadURL);


                firestore.collection('posts').add({
                    title: post.title,
                    content: post.content,
                    image: downloadURL, // Equivalent to post.title + post.content
                    authorFirstName: profile.firstName,
                    authorLastName: profile.lastName,
                    authorId: authorId,
                    createdAt: new Date()
                }).then(() => {
                    dispatch({ type: 'CREATE_POST', post});
                }).catch(err => {
                    dispatch({type: 'CREATE_POST_ERROR', err });
                });
            });
        });


        // Rerefence to 'posts' collection
        // ASYNC function which require then() method and catch(error) method
        
        
    }
}


