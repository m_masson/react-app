import React, { Component } from "react";
import { connect } from "react-redux";
import { newUser } from "../../store/actions/authActions";
import { Redirect } from "react-router-dom";


class SignUp extends Component {
  state = {
    email: "",
    password: "",
    firstName: "",
    lastName: "",
    role: ""
  };
  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    this.props.newUser(this.state);
  };
  render() {
    const { authError, auth } = this.props;
    if (!auth.uid) return <Redirect to="/" />;
    return (
      <div className="main container">
        <form onSubmit={this.handleSubmit} className="white">
          <h5 className="grey-text text-darken-3">Create new user</h5>
          <div className="input-field">
            <label htmlFor="email">Email</label>
            <input type="email" id="email" onChange={this.handleChange} required/>
          </div>
          <div className="input-field">
            <label htmlFor="password">Password</label>
            <input type="password" id="password" onChange={this.handleChange} required/>
          </div>
          <div className="input-field">
            <label htmlFor="firstName">First Name</label>
            <input type="text" id="firstName" onChange={this.handleChange} required/>
          </div>
          <div className="input-field">
            <label htmlFor="lastName">Last Name</label>
            <input type="text" id="lastName" onChange={this.handleChange} required/>
          </div>
          <div className="input-field">
            <label htmlFor="role">Role</label>
            <input type="text" id="role" onChange={this.handleChange} required/>
          </div>
          <div className="input-field">
            <button className="btn pink lighten-1 z-depth-0">Create</button>
            <div className="pink-text center">
                { authError ? <p>{authError}</p> : null }
            </div>
          </div>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    authError: state.auth.authError,
    auth: state.firebase.auth
  };
};
const mapDispatchToProps = (dispatch) => {
  // return represent what we want to attach to the prop
  return {
    newUser: (creds) => dispatch(newUser(creds))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
