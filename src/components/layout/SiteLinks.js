import React from 'react'
import { NavLink } from 'react-router-dom'



const SiteLinks = () => {
    return (
        <div className="right site-links">
            <ul className="right hide-on-med-and-down">
                <li><NavLink to='/post'>Blog</NavLink></li>
                <li><NavLink to='/activities'>Activités</NavLink></li>
                <li><NavLink to='/ressources'>Ressources</NavLink></li>
                <li><NavLink to='/about'>Qui nous sommes</NavLink></li>
            </ul>
        </div>
    )
}

export default SiteLinks;