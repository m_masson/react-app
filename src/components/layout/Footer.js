import React, { Component } from "react";


class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  
  render() {
    return (
        <footer className="page-footer">
            <div className="container">
            <div className="row">
                <div className="col m6 s12">
                <h5 className="white-text">AGEEI - UQAM</h5>
                <p className="grey-text text-lighten-4">Site internet de l'Association des étudiantes et étudiants en informatique de l'UQAM.</p>
                </div>
                <div className="col m4 offset-m2 s12">
                    <h5 className="white-text">Liens</h5>
                    <ul>
                        <li>
                            <a href="https://github.com/ageei">
                                <span><img className="icon" alt="github-icon" src="/img/github.png"/></span>
                                <span className="icon-name">Github</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://github.com/ageei">
                                <span><img className="icon" alt="github-icon" src="/img/twitter.png"/></span>
                                <span className="icon-name">Twitter</span>
                            </a>
                        </li>
                        <li>
                            <a href="mailto:executif@ageei.org">
                                <span><img className="icon" alt="github-icon" src="/img/email.png"/></span>
                                <span className="icon-name">executif@ageei.org</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            </div>
            {/* <div className="footer-copyright">
            <div className="container">
            
            <a className="grey-text text-lighten-4 right" href="https://ageei-uqam.slack.com/">SLACK</a>
            </div>
            </div> */}
        </footer>
    )
  }
}

export default Footer;
