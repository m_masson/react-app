import React, { Component } from 'react'
import {Link, withRouter } from 'react-router-dom'
import SignedInLinks from './SignedInLinks'
import SignedOutLinks from './SignedOutLinks'
import { connect } from 'react-redux'
import SiteLinks from './SiteLinks';
import MobileLinks from './MobileLinks';


document.onclick = () => {
    let x = document.querySelector('.user-choice');
    let y = document.querySelector('.mobile-menu');
    y.style.display = 'none';
    if (x) x.style.display = 'none';

}


class Navbar extends Component {
    
    componentDidMount() {
        let fixed = document.querySelector('#fixed');
        let nav = document.querySelector('#navbar');
        document.addEventListener('scroll', () => {
            if (window.scrollY > 64) {
                fixed.className = "navbar-fixed";
                nav.className = "grey darken-4";
            } else {
                fixed.className = "";
            }
        })
    }

    componentWillReceiveProps(nextProps) {
        let href= window.location.href;
        let origin = window.location.origin + '/';
        let nav = document.querySelector('#navbar');
        if (href !== origin ) {
            nav.className = "grey darken-4";
        }
    }
    
    render () {
        const { auth, profile } = this.props;
        const links = auth.uid ? <SignedInLinks profile={profile}/> : <SignedOutLinks/>;    
        return (
            <div id="fixed">
                <nav id="navbar">
                    <div className="nav-wrapper nav-container">
                        <div className="relative">
                            <Link to='/' className="brand-logo left"><img className="logo" alt="logo" src="img/Logo.png" atl="logo"/></Link>
                            { links }
                            <SiteLinks/>
                            <MobileLinks/>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}


// const Navbar = (props) => {

//     const { auth, profile } = props;
//     const links = auth.uid ? <SignedInLinks profile={profile}/> : <SignedOutLinks/>;
//     return (
//         <div id="fixed">
//             <nav id="navbar">
//                 <div className="nav-wrapper nav-container">
//                     <div className="relative">
//                         <Link to='/' className="brand-logo left"><img className="logo" alt="logo" src="img/Logo.png" atl="logo"/></Link>
//                         { links }
//                         <SiteLinks/>
//                         <MobileLinks/>
//                     </div>
//                 </div>
//             </nav>
//         </div>
//     )
// }

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth,
        profile: state.firebase.profile
    }
}
export default withRouter(connect(mapStateToProps)(Navbar));
