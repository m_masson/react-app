import React from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { signOut } from '../../store/actions/authActions'


const SignedInLinks = (props) => {
    const handleClick = (e) => {
        e.preventDefault();
        let x = document.querySelector('.user-choice');
        x.style.display === 'block' ? x.style.display = 'none' : x.style.display = 'block';
    }
    return (
        <div className="right relative" >
            <button to='/' className='btn btn-floating indigo lighten-4' onClick={handleClick}>{props.profile.initials}</button>
            <ul className="user-choice grey darken-4">
                <li><NavLink to='/newuser'>Create User</NavLink></li>
                <li><NavLink to='/create'>New Post</NavLink></li>
                <li><a href="#!" onClick={props.signOut}>Log Out</a></li>
            </ul>
        </div>
        // <ul className="right">
        //     <li><NavLink to='/newuser'>Create User</NavLink></li>
        //     <li><NavLink to='/create'>New Post</NavLink></li>
        //     <li><a onClick={props.signOut}>Log Out</a></li>
        //     <li><NavLink to='/' className='btn btn-floating pink lighten-1'>{props.profile.initials}</NavLink></li>
        // </ul>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(signOut())
    }
}

export default connect(null, mapDispatchToProps)(SignedInLinks);
