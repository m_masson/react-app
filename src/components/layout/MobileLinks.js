import React from 'react'
import { NavLink } from 'react-router-dom'



const MobileLinks = () => {

    const handleClick = (e) => {
        e.preventDefault();
        let y = document.querySelector('.mobile-menu');
        y.style.display === 'block' ?  y.style.display = 'none' : y.style.display = 'block';
    }  
    

    return (
        <div className="right relative" >
            <i className="large material-icons white-text hide-on-large-only right mobile" onClick={handleClick}>apps</i>
            <ul className="right mobile-menu hide-on-large-only grey darken-4">
                <li><NavLink to='/post'>Blog</NavLink></li>
                <li><NavLink to='/activities'>Activités</NavLink></li>
                <li><NavLink to='/ressources'>Ressources</NavLink></li>
                <li><NavLink to='/about'>Qui nous sommes</NavLink></li>
            </ul>
        </div>
        // <ul className="right">
        //     <li><NavLink to='/newuser'>Create User</NavLink></li>
        //     <li><NavLink to='/create'>New Post</NavLink></li>
        //     <li><a onClick={props.signOut}>Log Out</a></li>
        //     <li><NavLink to='/' className='btn btn-floating pink lighten-1'>{props.profile.initials}</NavLink></li>
        // </ul>
    )
}

export default MobileLinks;