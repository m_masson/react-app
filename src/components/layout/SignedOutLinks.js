import React from 'react'
import { NavLink } from 'react-router-dom'

const SignedOutLinks = () => {
    return (
        <ul className="right">
            {/* <li><NavLink to='/'>Signup</NavLink></li> */}
            <li className="indigo lighten-4"><NavLink to='/signin'>Login</NavLink></li>
        </ul>
    )
}

export default SignedOutLinks;
