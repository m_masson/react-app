import React, { Component } from 'react'

class About extends Component {
  render() {
    return (
      <div className="main container">
      <h1>Qui sommes-nous ?</h1>
      <p>L’association générale des étudiantes et étudiants en informatique a pour but de regrouper les étudiants en informatique de l’Université du Québec à Montréal et d’assurer, dans le cadre des affaires universitaires, leur bienêtre physique, moral, intellectuel, social et économique.</p>
      <img src="https://firebasestorage.googleapis.com/v0/b/blog-app-a3030.appspot.com/o/Beslogic.jpg?alt=media&token=c98ad472-025a-4d21-8902-121d0e5b4094" alt="beslogic.jpg" className="img-header"/>
      </div>
    )
  }
}

export default About;