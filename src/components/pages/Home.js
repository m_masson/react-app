import React, { Component } from "react";

const scrollDown = e => {
  e.preventDefault();
  var element = document.getElementById("section1");
  element.scrollIntoView({ behavior: "smooth", block: "start" });
};
const fit = {
    height: '100%',
    width: '100%',
    objectFit: 'contain',
    verticalAlign: 'middle'
}
const buttonSlack = {
    marginTop: '48px'
}
const textWidth = {
  maxWidth: '90%',
  margin: '0 auto'
}
class Home extends Component {

  render() {
    return (
      <div>
        <div className="landing">
          <div className="landing-img animated fadeIn">
            <div className="wrapper">
              <img src="img/slack.png" alt="iphone" />
              <span className="landing-ellipse animated infinite pulse" />
            </div>
          </div>
          <div className="landing-title animated fadeInLeft">
            <h2>AGEEI</h2>
            <p>
              Association Générale
              <br />
              des Étudiantes & Étudiants
              <br />
              en Informatique
            </p>
          </div>
          <div className="landing-slack animated fadeInLeft delay-1s hide-on-small-only">
            <h4>Slack</h4>
            <span className="sub-text">
              Avec notre plateforme Slack
              <br />
              tu pourra apprendre, partager
              <br />
              aider et socialiser
            </span>
          </div>
          <div className="landing-fractal animated fadeInRight delay-1s hide-on-small-only">
            <h4>Sain Fractal</h4>
            <span className="sub-text">
              Notre pavillon est menu du meilleur
              <br />
              des café étudiants de l'UQAM. Café pas cher...
              <br />
              et pafois bière pas cher !
            </span>
          </div>
          <div className="landing-visit">
            <div onClick={scrollDown}>
              <span className="ellipse" />
              EXPLORER
            </div>
          </div>
        </div>
        <div id="section1">
            <div className="row">
                <div className="col l6 m12 center-align">
                  <div style={textWidth}>
                    <h3 className="text-ageei">Apprendre Partager Socialiser</h3>
                    <h5 className="hide-on-med-and-down">Slack est sans doute la plateforme qui est la plus utilisée dans le département. Nous sommes chanceux - même les enseignants l'utilisent !</h5>
                    <h6 className="hide-on-med-and-down">Nous rendons accessible à tous les étudiants les channels de chaque cours enseignés du programme de Génie Logiciel & Informatique. En plus, on crée même des channels pour socialiser !</h6>
                    <img className="hide-on-large-only" style={fit} src="img/Slack.jpg" alt="slackTemplate"/>
                    <a style={buttonSlack} className="waves-effect waves-light btn indigo lighten-5 black-text pulse " href="https://ageei-uqam.slack.com/"><i className="material-icons left">cloud</i>Rejoindre</a>
                  </div>
                </div>
                <div className="col l6 m12 hide-on-med-and-down">
                    <img style={fit} src="img/Slack.jpg" alt="slackTemplate"/>
                </div>
            </div>
        
        </div>
      </div>
    );
  }
}

export default Home;
