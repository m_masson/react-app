import React, {Component} from 'react';
import LastNews from './LastNews';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import PostLists from '../projects/PostLists';


class Dashboard extends Component {

    render() {
        const { posts, lastnews } = this.props;

        return (
            <div className="main dashboard">
                <div className="">
                    <div className="row">
                        <div className="col m3 offset-m1 s12 hide-on-small-only">
                            <LastNews lastnews={lastnews}/>
                        </div>
                        <div className="col m8 s12">
                            <PostLists posts={posts}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s12">
                            <ul class="pagination">
                                <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
                                <li class="active"><a href="#!">1</a></li>
                                <li class="waves-effect"><a href="#!">2</a></li>
                                <li class="waves-effect"><a href="#!">3</a></li>
                                <li class="waves-effect"><a href="#!">4</a></li>
                                <li class="waves-effect"><a href="#!">5</a></li>
                                <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts: state.firestore.ordered.posts,
        auth: state.firebase.auth,
        lastnews: state.firestore.ordered.lastnews
    }
}
export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        { collection: 'posts', orderBy: ['createdAt', 'desc'] },
        { collection: 'lastnews', limit: 3, orderBy: ['time', 'desc'] },
        { collection: 'users' }
    ])
)(Dashboard);
