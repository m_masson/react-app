import React from 'react'
import PostSummary from './PostSummary'
import { Link } from 'react-router-dom'


const PostLists = ({posts}) => {
    return (
        <div className="post-list section">
            {/* Verify if there's post at first */ }
            { posts && posts.map(post => {
                return (
                    <div className="col l4 m6 s12">
                    <Link to={'/post/' + post.id} key={post.id}>
                        <PostSummary post={post}/>
                    </Link>
                    </div>
                )
            })}
        </div>
    )
}

export default PostLists;