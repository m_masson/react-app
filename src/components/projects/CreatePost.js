import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createPost } from '../../store/actions/postActions';
import { Redirect } from 'react-router-dom';

const img = {
    width: '100%',
    marginBottom: '16px',
    maxHeight: 350,
    objectFit: 'cover',
    display: 'none'
}

class CreatePost extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            content: '',
            file: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange = (e) => {
        if ([e.target.id][0] === 'title' || [e.target.id][0] === 'content') {
            this.setState({
                [e.target.id]: e.target.value
            })
        } else {
            this.setState({
                [e.target.id]: e.target.files[0]
            })
            let input = document.querySelector('#file');
            readURL(input);
            changeHTML(e);
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        let form = document.querySelector('#form');
        let size = this.state.file.size / 1000000;
        if (size > 5) {
            alert('File is to big ! 5mg max');
        } else {
            if (this.state.file.type.split('/')[0] !== 'image') { 
                alert('File must be an image');
            } else {
                this.props.createPost(this.state);
                this.props.history.push('/post');
                form.reset();
            }
        }
    }
  render() {
      const { auth } = this.props;
      if (!auth.uid) return <Redirect to='/'/>
    return (
      <div className="main container">
        <form onSubmit={this.handleSubmit} id="form" className="white">
            <h5 className="grey-text text-darken-3">New Post</h5>
            <img style={img} id="preview" alt="preview" src="#"/>
            <div className="input-field">
                <label htmlFor="title">Title</label>
                <input type="text" id="title" onChange={this.handleChange} required/>
            </div>
            <div className="input-field">
                <label htmlFor="content">Post Content</label>
                <textarea className="materialize-textarea" id="content" onChange={this.handleChange} required></textarea>
            </div>
            <input type="file" id="file" className="input-file" onChange={this.handleChange}/>
            <label htmlFor="file" className="waves-effect waves-light btn indigo lighten-5 grey-text">Add image</label>
            <div className="input-field">

                <button className="btn pink lighten-1 z-depth-0">Create</button>
            </div>
        </form>
      </div>
    )
  }
}

const changeHTML = (e) => {
    const input = document.querySelector( '.input-file' );
    var label = input.nextElementSibling,
    labelVal = label.innerHTML;
    var fileName = e.target.value.split('\\').pop();
    if (fileName) {
        label.innerHTML = fileName;
    } else {
        label.innerHTML = labelVal;
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        createPost: (post) => dispatch(createPost(post)),
    }
}

const readURL = (input) => {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
  
      reader.onload = function(e) {
        document.getElementById('preview').src =  e.target.result;
        document.getElementById('preview').style.display = 'block';
      }
      reader.readAsDataURL(input.files[0]);
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreatePost);
