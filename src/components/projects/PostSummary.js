import React from "react";
import moment from 'moment';

const padding = {
  padding: '0px'
}
const PostSummary = ({post}) => {
  return (
    <div style={padding} className="card z-depth-0 post-summary">
      <div style={padding} className="card-content grey-text text-darken-3">
        <div className="thumbnail-wrapper">
        <img className="img-fit" src={post.image} alt="blog-img"/>
        </div>
        <span className="card-title">{post.title}</span>
        <p>Posted by {post.authorFirstName} {post.authorLastName}</p>
        <p className="grey-text">{moment(post.createdAt.toDate()).calendar()}</p>
      </div>
    </div>
  );
};

export default PostSummary;