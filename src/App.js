import React from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Navbar from './components/layout/Navbar';
import Footer from './components/layout/Footer';
import Dashboard from './components/dashboard/Dashboard'
import SignIn from './components/auth/SignIn'
import SignUp from './components/auth/SignUp'
import PostDetails from './components/projects/PostDetails';
import CreatePost from './components/projects/CreatePost';
import Activite from './components/pages/Activite';
import About from './components/pages/About';
import Ressources from './components/pages/Ressources';
import Home from './components/pages/Home';


function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar/>
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/post' exact component={Dashboard} />
            <Route path='/post/:id' component={PostDetails}/>
            <Route path='/signin' component={SignIn}/>
            <Route path='/create' component={CreatePost}/>
            <Route path='/newuser' component={SignUp}/>
            <Route path='/activities' component={Activite}/>
            <Route path='/about' component={About}/>
            <Route path='/ressources' component={Ressources}/>
          </Switch>
          <Footer/>
      </div>
      
    </BrowserRouter>
  );
}

export default App;
